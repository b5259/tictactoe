from tkinter import *
from tkinter import messagebox
root = Tk()
root.title('TIC TAC TOE')
#X starts so true
clicked = True
count = 0
#disable buttons
def disable_all_buttons():
    button1.config(state=DISABLED)
    button2.config(state=DISABLED)
    button3.config(state=DISABLED)
    button4.config(state=DISABLED)
    button5.config(state=DISABLED)
    button6.config(state=DISABLED)
    button7.config(state=DISABLED)
    button8.config(state=DISABLED)
    button9.config(state=DISABLED)
#check to winning
def checkifwon():
    global winner
    winner = False
    #Winning  cases of X
    if button1["text"] == "X" and button2["text"] == "X" and button3["text"] == "X":
        button1.config(bg="green")
        button2.config(bg="green")
        button3.config(bg="green")
        winner = True
        messagebox.showinfo("TIC TAC TOE","CONGRATULATIONS X WINS!!")
        disable_all_buttons()
    elif button4["text"] == "X" and button5["text"] == "X" and button6["text"] == "X":
        button4.config(bg="green")
        button5.config(bg="green")
        button6.config(bg="green")
        winner = True
        messagebox.showinfo("TIC TAC TOE","CONGRATULATIONS X WINS!!")
        disable_all_buttons()
    elif button7["text"] == "X" and button8["text"] == "X" and button9["text"] == "X":
        button7.config(bg="green")
        button8.config(bg="green")
        button9.config(bg="green")
        winner = True
        messagebox.showinfo("TIC TAC TOE","CONGRATULATIONS X WINS!!")
        disable_all_buttons()
    elif button1["text"] == "X" and button4["text"] == "X" and button7["text"] == "X":
        button1.config(bg="green")
        button4.config(bg="green")
        button7.config(bg="green")
        winner = True
        messagebox.showinfo("TIC TAC TOE","CONGRATULATIONS X WINS!!")
        disable_all_buttons()
    elif button2["text"] == "X" and button5["text"] == "X" and button8["text"] == "X":
        button2.config(bg="green")
        button5.config(bg="green")
        button8.config(bg="green")
        winner = True
        messagebox.showinfo("TIC TAC TOE", "CONGRATULATIONS X WINS!!")
        disable_all_buttons()
    elif button3["text"] == "X" and button6["text"] == "X" and button9["text"] == "X":
        button3.config(bg="green")
        button6.config(bg="green")
        button9.config(bg="green")
        winner = True
        messagebox.showinfo("TIC TAC TOE","CONGRATULATIONS X WINS!!")
        disable_all_buttons()
    elif button1["text"] == "X" and button5["text"] == "X" and button9["text"] == "X":
        button1.config(bg="red")
        button5.config(bg="red")
        button9.config(bg="red")
        winner = True
        messagebox.showinfo("TIC TAC TOE","CONGRATULATIONS X WINS!!")
        disable_all_buttons()
    elif button3["text"] == "X" and button5["text"] == "X" and button7["text"] == "X":
        button3.config(bg="green")
        button5.config(bg="green")
        button7.config(bg="green")
        winner = True
        messagebox.showinfo("TIC TAC TOE","CONGRATULATIONS X WINS!!")
        disable_all_buttons()
     elif (buttons1["text"] == "X" and buttons2["text"] =="X" and buttons3 =="X"or
          buttons4["text"] == "X" and buttons5["text"] =="X" and buttons6 =="X" or
          buttons7["text"] == "X" and buttons8["text"] =="X" and buttons9 =="X" or
          buttons3["text"] == "X" and buttons5["text"] =="X" and buttons7 =="X" or
          buttons1["text"] == "X" and buttons5["text"] =="X" and buttons9 =="X" or
          buttons1["text"] == "X" and buttons4["text"] =="X" and buttons7 =="X" or
          buttons2["text"] == "X" and buttons5["text"] =="X" and buttons8 =="X" or
          buttons3["text"] == "X" and buttons6["text"] =="X" and buttons9 =="X"):
          tkinter.messagebox.showinfo("Winner X","X won a game")
     elif (buttons1["text"] == "O" and buttons2["text"] =="O" and buttons3 =="O" or
          buttons4["text"] == "O" and buttons5["text"] =="O" and buttons6 =="O" or
          buttons7["text"] == "O" and buttons8["text"] =="O" and buttons9 =="O" or
          buttons3["text"] == "O" and buttons5["text"] =="O" and buttons7 =="O" or
          buttons1["text"] == "O" and buttons5["text"] =="O" and buttons9 =="O" or
          buttons1["text"] == "O" and buttons4["text"] =="O" and buttons7 =="O" or
          buttons2["text"] == "O" and buttons5["text"] =="O" and buttons8 =="O" or
          buttons3["text"] == "O" and buttons6["text"] =="O" and buttons9 =="O"):
          tkinter.messagebox.showinfo("Winner X","X won a game")
    #Winning cases of O
    elif button1["text"] == "O" and button2["text"] == "O" and button3["text"] == "O":
        button1.config(bg="green")
        button2.config(bg="green")
        button3.config(bg="green")
        winner = True
        messagebox.showinfo("TIC TAC TOE", "CONGRATULATIONS O WINS!!")
        disable_all_buttons()
    elif button4["text"] == "O" and button5["text"] == "O" and button6["text"] == "O":
        button4.config(bg="green")
        button5.config(bg="green")
        button6.config(bg="green")
        winner = True
        messagebox.showinfo("TIC TAC TOE", "CONGRATULATIONS O WINS!!")
        disable_all_buttons()
    elif button7["text"] == "O" and button8["text"] == "O" and button9["text"] == "O":
        button7.config(bg="red")
        button8.config(bg="red")
        button9.config(bg="red")
        winner = True
        messagebox.showinfo("TIC TAC TOE", "CONGRATULATIONS O WINS!!")
        disable_all_buttons()
    elif button1["text"] == "O" and button4["text"] == "O" and button7["text"] == "O":
        button1.config(bg="green")
        button4.config(bg="green")
        button7.config(bg="green")
        winner = True
        messagebox.showinfo("TIC TAC TOE", "CONGRATULATIONS O WINS!!")
        disable_all_buttons()
    elif button2["text"] == "O" and button5["text"] == "O" and button8["text"] == "O":
        button2.config(bg="green")
        button5.config(bg="green")
        button8.config(bg="green")
        winner = True
        messagebox.showinfo("TIC TAC TOE", "CONGRATULATIONS O WINS!!")
        disable_all_buttons()
    elif button3["text"] == "O" and button6["text"] == "O" and button9["text"] == "O":
        button3.config(bg="green")
        button6.config(bg="green")
        button9.config(bg="green")
        winner = True
        messagebox.showinfo("TIC TAC TOE", "CONGRATULATIONS O WINS!!")
        disable_all_buttons()
    elif button1["text"] == "O" and button5["text"] == "O" and button9["text"] == "O":
        button1.config(bg="green")
        button5.config(bg="green")
        button9.config(bg="green")
        winner = True
        messagebox.showinfo("TIC TAC TOE", "CONGRATULATIONS O WINS!!")
        disable_all_buttons()
    elif button3["text"] == "0" and button5["text"] == "0" and button7["text"] == "0":
        button3.config(bg="green")
        button5.config(bg="green")
        button7.config(bg="green")
        winner = True
        messagebox.showinfo("TIC TAC TOE", "CONGRATULATIONS 0 WINS!!")
        disable_all_buttons()
    #check if tie
    if count == 9 and winner == False:
        messagebox.showinfo("TIC TAC TOE","IT'S A TIE!\n NO ONE WINS!")
        disable_all_buttons()
#button clicked function
def b_click(button):
    global clicked,count
    if button["text"] == " " and clicked == True:
        button["text"] = "X"
        clicked = False
        count += 1
        checkifwon()
    elif button["text"] == " " and clicked == False:
        button["text"] = "O"
        clicked = True
        count += 1
        checkifwon()
    else:
        messagebox.showerror("TIC TAC TOE","That box has already been selected\n pick another line")
#Start the game over!
def reset():
    global button1,button2,button3,button4,button5,button6,button7,button8,button9
    global clicked,count
    clicked = True
    count = 0
#bulding of buttons
    button1 = Button(root, text=" ", font=("Helvetica", 20), height=3, width=6, bg="red", command=lambda: b_click(button1))
    button2 = Button(root, text=" ", font=("Helvetica", 20), height=3, width=6, bg="red", command=lambda: b_click(button2))
    button3 = Button(root, text=" ", font=("Helvetica", 20), height=3, width=6, bg="red", command=lambda: b_click(button3))
    button4 = Button(root, text=" ", font=("Helvetica", 20), height=3, width=6, bg="red", command=lambda: b_click(button4))
    button5 = Button(root, text=" ", font=("Helvetica", 20), height=3, width=6, bg="red", command=lambda: b_click(button5))
    button6 = Button(root, text=" ", font=("Helvetica", 20), height=3, width=6, bg="red", command=lambda: b_click(button6))
    button7 = Button(root, text=" ", font=("Helvetica", 20), height=3, width=6, bg="red", command=lambda: b_click(button7))
    button8 = Button(root, text=" ", font=("Helvetica", 20), height=3, width=6, bg="red", command=lambda: b_click(button8))
    button9 = Button(root, text=" ", font=("Helvetica", 20), height=3, width=6, bg="red", command=lambda: b_click(button9))
# attachment buttons 
    button1.grid(row=0, column=0)
    button2.grid(row=0, column=1)
    button3.grid(row=0, column=2)
    button4.grid(row=1, column=0)
    button5.grid(row=1, column=1)
    button6.grid(row=1, column=2)
    button7.grid(row=2, column=0)
    button8.grid(row=2, column=1)
    button9.grid(row=2, column=2)
#create menu
my_menu = Menu(root)
root.config(menu=my_menu)
#create options menu
Options_menu = Menu(my_menu,tearoff = False)
my_menu.add_cascade(label="Options",menu = Options_menu)
Options_menu.add_command(label = "NEW GAME",command= reset)
reset()
root.mainloop()
